"""
    Designed Map
    1       2       3       4       5       6       7
         ┌ ─ ─ ─ ┬ ─ ─ ─ ┬ ─ ─ ─ ┬ ─ ─ ─ ┬ ─ ─ ─ ┬ ─ ─ ─ ┬ ─ ─ ─ ┐
         │       │       │││││││││││││││││       │       │││││││││
         │       │       │││││││││││││││││       │       │││││││││
   1     │       │       │││││││││││││││││       │       │││││││││
         │││││││││       │││││││││       │       │││││││││││││││││
         │││││││││       │││││││││       │       │││││││││││││││││
   2     │││││││││       │││││││││       │       │││││││││││││││││
         │││││││││       │││││││││││││││││││││││││││││││││       │
         │││││││││       │││││││││││││││││││││││││││││││││       │
   3     │││││││││       │││││││││││││││││││││││││││││││││       │
         │││││││││││││││││││││││││       │       │││││││││       │
         │││││││││││││││││││││││││       │       │││││││││       │
   4     │││││││││││││││││││││││││       │       │││││││││       │
         │││││││││       │││││││││││││││││       │       │       │
         │││││││││       │││││││││││││││││       │       │       │
   5     │││││││││       │││││││││││││││││       │       │       │
         └ ─ ─ ─ ┴ ─ ─ ─ ┴ ─ ─ ─ ┴ ─ ─ ─ ┴ ─ ─ ─ ┴ ─ ─ ─ ┴ ─ ─ ─ ┘
"""


class move_position:
    ZONENAME = ''
    DESCRIPTION = 'description'
    EXAMINATION = 'examination'

    # When you solved mission, it'll changes 'True'.
    SOLVED = False
    UP = 'up', 'north', ''
    DOWN = 'down', 'south', ''
    LEFT = 'left', 'west', ''
    RIGHT = 'right', 'east', ''

    # User cannot go there because there are no space
    # that User go.
    BLOCKED = False
    x = 7
    y = 5
    # a = [[0] * x] * y
    # a[0][0] = False

    # x_rows = [1, 2, 3, 4, 5, 6, 7]
    # y_rows = [1, 2, 3, 4, 5]
    # print(a[0][0])

    # Set the location by controlling this values.
    # I did not attach to the dictionary list.
    player_pos = [
        1, 2, 3, 4, 5, 6, 7,
        11, 12, 13, 14, 15, 16, 17,
        21, 22, 23, 24, 25, 26, 27,
        31, 32, 33, 34, 35, 36, 37,
        41, 42, 43, 44, 45, 46, 47
    ]

    if player_pos in [
        41, 31, 21, 11, 32, 3, 13, 23, 33, 43,
        4, 24, 44, 25, 16, 26, 36, 7, 17
    ]:
        print("It is the places where are allowed to access.")
    elif player_pos in [
        1, 2, 12, 22, 42, 14, 34,
        5, 15, 35, 45, 6, 46, 27, 37, 47
    ]:
        print("It is the places where are prohibited to access.")

    player_pos[27] = {
        """
            Information of Start,
            Because it has 27 list-values, so I've just set the range
            as '27'.
        """
    }
    # print(player_pos)
    # '' is the attribute place where player cannot go.
    solved_places = {
        '': False,
        '1,1': False, '1,2': False, '1,3': False, '1,4': False, '1,5': False, '1,6': False, '1,7': False,
        '2,1': False, '2,2': False, '2,3': False, '2,4': False, '2,5': False, '2,6': False, '2,7': False,
        '3,1': False, '3,2': False, '3,3': False, '3,4': False, '3,5': False, '3,6': False, '3,7': False,
        '4,1': False, '4,2': False, '4,3': False, '4,4': False, '4,5': False, '4,6': False, '4,7': False,
        '5,1': False, '5,2': False, '5,3': False, '5,4': False, '5,5': False, '5,6': False, '5,7': False,
    }

    map_zone = {
        '1,1': {
            'ZONENAME': '11',
            'DESCRIPTION': "This is the town's entrance",
            'EXAMINATION': 'examination',
            'SOLVED': False,
            BLOCKED: True,
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
        },
        '1,2': {
            'ZONENAME': '12',
            'DESCRIPTION': 'description',
            'EXAMINATION': 'examination',
            'SOLVED': False,
            BLOCKED: True,
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
        },
        '1,3': {
            'ZONENAME': 'SKELETON_WARRIOR_2',
            'DESCRIPTION': 'description',
            'EXAMINATION': 'examination',
            'SOLVED': False,
            BLOCKED: False,
            UP: '',
            DOWN: '2,3',
            LEFT: '',
            RIGHT: '1,4'
        },
        '1,4': {
            'ZONENAME': 'BOSS_KEY',
            'DESCRIPTION': 'description',
            'EXAMINATION': 'examination',
            'SOLVED': False,
            BLOCKED: False,
            UP: '',
            DOWN: '',
            LEFT: '1,3',
            RIGHT: ''
        },

        '1,5': {
            'ZONENAME': '15',
            'DESCRIPTION': 'description',
            'EXAMINATION': 'examination',
            'SOLVED': False,
            BLOCKED: True,
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: ''
        },
        '1,6': {
            'ZONENAME': '16',
            'DESCRIPTION': 'description',
            'EXAMINATION': 'examination',
            'SOLVED': False,
            BLOCKED: True,
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: ''
        },
        '1,7': {
            'ZONENAME': 'end',
            'DESCRIPTION': 'description',
            'EXAMINATION': 'examination',
            'SOLVED': False,
            BLOCKED: True,
            UP: '',
            DOWN: '2,7',
            LEFT: '',
            RIGHT: ''
        },
        '2,1': {
            'ZONENAME': '21',
            'DESCRIPTION': 'description',
            'EXAMINATION': 'examination',
            'SOLVED': False,
            BLOCKED: True,
            UP: '',
            DOWN: '3,1',
            LEFT: '',
            RIGHT: ''
        },
        '2,2': {
            'ZONENAME': "22",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True,
        },
        '2,3': {
            'ZONENAME': "23",
            'DESCRIPTION': "Hallway",
            'EXAMINATION': "example",
            UP: '1,3',
            DOWN: '3,3',
            LEFT: '',
            RIGHT: '',
            'SOLVED': True,
            BLOCKED: False,
        },
        '2,4': {
            'ZONENAME': "24",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True,
        },
        '2,5': {
            'ZONENAME': "25",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True,
        },
        '2,6': {
            'ZONENAME': "26",
            'DESCRIPTION': "Hallway",
            'EXAMINATION': "example",
            UP: '',
            DOWN: '3,6',
            LEFT: '',
            RIGHT: '2,7',
            'SOLVED': True,
            BLOCKED: False,
        },
        '2,7': {
            'ZONENAME': "SKELETON_KING",
            'DESCRIPTION': "",
            'EXAMINATION': "example",
            UP: '1,7',
            DOWN: '',
            LEFT: '2,6',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: False,
        },
        '3,1': {
            'ZONENAME': "HEALTH_POTION",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '2,1',
            DOWN: '4,1',
            LEFT: '',
            RIGHT: '',
            'SOLVED': True,
            BLOCKED: False,
        },
        '3,2': {
            'ZONENAME': "",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True,
        },
        '3,3': {
            'ZONENAME': "33",
            'DESCRIPTION': "Hallway",
            'EXAMINATION': "example",
            UP: '2,3',
            DOWN: '4,3',
            LEFT: '',
            RIGHT: '3,4',
            'SOLVED': True,
            BLOCKED: False,
        },
        '3,4': {
            'ZONENAME': "HEALTH_POTION",
            'DESCRIPTION': "",
            'EXAMINATION': "example",
            UP: '',
            DOWN: '',
            LEFT: '3,3',
            RIGHT: '3,5',
            'SOLVED': False,
            BLOCKED: False,
        },
        '3,5': {
            'ZONENAME': "SKELETON_WARRIOR_4",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '3,4',
            RIGHT: '3,6',
            'SOLVED': False,
            BLOCKED: False,
        },
        '3,6': {
            'ZONENAME': "Hallway",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '2,6',
            DOWN: '4,6',
            LEFT: '3,5',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: False,
        },
        '3,7': {
            'ZONENAME': "",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True,
        },
        '4,1': {
            'ZONENAME': "Hallway",
            'DESCRIPTION': "description",
            'EXAMINATION': "example",
            UP: '3,1',
            DOWN: '5,1',
            LEFT: '',
            RIGHT: '4,2',
            'SOLVED': True,
            BLOCKED: False,
        },
        '4,2': {
            'ZONENAME': "SKELETON_WARRIOR_1",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '4,1',
            RIGHT: '4,3',
            'SOLVED': False,
            BLOCKED: False,
        },
        '4,3': {
            'ZONENAME': "Hallway",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '3,3',
            DOWN: '5,3',
            LEFT: '4,2',
            RIGHT: '',
            'SOLVED': True,
            BLOCKED: False,
        },
        '4,4': {
            'ZONENAME': "",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True
        },
        '4,5': {
            'ZONENAME': "",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True
        },
        '4,6': {
            'ZONENAME': "SPEED BOOTS",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '3,6',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True
        },
        '4,7': {
            'ZONENAME': "",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True
        },
        '5,1': {
            'ZONENAME': "START",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '4,1',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': True,
            BLOCKED: False
        },
        '5,2': {
            'ZONENAME': "",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True
        },
        '5,3': {
            'ZONENAME': "SKELETON_WARRIOR_3",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '4,3',
            DOWN: '',
            LEFT: '',
            RIGHT: '5,4',
            'SOLVED': False,
            BLOCKED: False,
        },
        '5,4': {
            'ZONENAME': "IRON_SWORD",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '5,3',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: False,
        },
        '5,5': {
            'ZONENAME': "",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True
        },
        '5,6': {
            'ZONENAME': "",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True
        },
        '5,7': {
            'ZONENAME': "",
            'DESCRIPTION': "description",
            'EXAMINATION': "examine",
            UP: '',
            DOWN: '',
            LEFT: '',
            RIGHT: '',
            'SOLVED': False,
            BLOCKED: True
        }

    }
