"""
    Could not separate 'Player' Class
    because it has initializer-Function.

    Set the default-job as 'warrior'.
"""
import random

weapons = {
    "IRON SWORD": 20
}

shields = {
    "IRON SHIELD": 20
}

speeds = {
    "SPEED BOOTS": 10
}


class Player:
    isUserHaveAKey = False

    def __init__(self, name, job):
        self.name = name
        self.job = job
        self.max_hp = 60
        self.hp = self.max_hp
        self.ap = 10
        # self.defence = 5
        self.speed = 10
        self.status_effects = []
        self.location = '5,1'
        self.boss_key = 'BOSS KEY'

        # We don't need to use 'gold'
        # because We've set the 'Default Weapon and Shield'
        # as 'IRON WEAPON and IRON SHIELD'.

        # self.gold = 40
        # self.weapon = ["normal_weapon"]
        self.current_weapon = ["IRON WEAPON"]
        # self.shield = ["normal_shield"]
        self.current_shield = ["IRON SHIELD"]
        # self.game_over = 0

    # This is the codes where Player hits
    @property
    def attack(self):
        attack = self.ap
        # if self.current_weapon == "normal_weapon":
        #     attack += 10
        if self.current_weapon == "IRON WEAPON":
            attack += 20

        return attack

    @property
    def defence(self):
        defence = self.hp
        if self.current_shield == "IRON SHIELD":
            defence -= 1

        return defence


class Skeleton_Warrior:
    def __init__(self, name, location):
        self.name = name

        # Health
        self.max_hp = 25
        self.hp = self.max_hp

        # Attack
        self.ap = 10
        self.defence = 0
        self.speed = 5
        self.location = location


class Skeleton_King:
    def __init__(self, name, location):
        self.name = name

        # Health
        self.max_hp = 40
        self.hp = self.max_hp

        self.ap = 12
        self.defence = 5
        self.speed = 15
        self.location = location


"""
    Fighting position between User and Enemy.
"""


class fight:
    FIGHT = ""
    RUN = ""
    USE_POTION = ""

# Enemy hits player.
# def playerAttack():

# Player his Enemy.
# def playerAttack():


