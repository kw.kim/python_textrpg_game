import os
import sys
import time

# from User_Information.user_name import player_name_attribute
from User_Position import user_position
from User_Position.user_position import *
# from User_Information.user_information import inform_Player
from Player_Information.player import *

screen_width = 100

# Show Player Position
char_pos = move_position()

# Set enemies and the king of enemies position.
# inform_Player()
# inform_Player = Player()

mswarrior = [
    Skeleton_Warrior('SKELETON_WARRIOR_1', '4,2'),
    Skeleton_Warrior('SKELETON_WARRIOR_2', '1,3'),
    Skeleton_Warrior('SKELETON_WARRIOR_3', '5,3'),
    Skeleton_Warrior('SKELETON_WARRIOR_4', '3,5')
]
mSWarrior = Skeleton_Warrior('SKELETON_WARRIOR_1', '4,2')
mSWarrior2 = Skeleton_Warrior('SKELETON_WARRIOR_2', '1,3')
mSWarrior3 = Skeleton_Warrior('SKELETON_WARRIOR_3', '5,3')
mSWarrior4 = Skeleton_Warrior('SKELETON_WARRIOR_4', '3,5')
mSKing = Skeleton_King('SKELETON_WARRIOR_4', '2,7')

# For attack and defence from enemies attack.


"""
    Collected basic-attributes of this project.
"""


# Generated 'clear' function to clear current Screen.
def clear():
    # os.system('clear')
    print("\n")


def movement_handler(destination):
    print("\n" + "You have moved to the" + destination + ".")
    inform_Player.location = destination
    print_location()


def win():
    print("You win!!")
    # eAttack.hp = 0
    # eAttack_2.hp = 0
    # eAttack_3.hp = 0
    # eAttack_boss.hp = 0
    # print("You have defeated the %s" % eAttack.name)
    prompt()


def die():
    clear()
    print("You've died!!")
    option = input("> ")


def attack_defence_eAttack_information():
    global eAttack, eAttack_1, eAttack_2, eAttack_3, eAttack_boss
    eAttack = mSWarrior
    eAttack_1 = mSWarrior2
    eAttack_2 = mSWarrior3
    eAttack_3 = mSWarrior4

    # Player's HP Lost = Player's attack - Player's defence
    # As a Result, Player's defence value become '5'.
    # eAttack = random.randint(int(mSWarrior.ap / 10), int(mSWarrior.ap - 5))
    eAttack = random.randint(0, 5)
    eAttack_1 = random.randint(0, 5)
    eAttack_2 = random.randint(0, 5)
    eAttack_3 = random.randint(0, 5)
    eAttack_boss = random.randint(0, 5)


def attack():
    clear()

    attack_defence_eAttack_information()

    pAttack = random.randint((inform_Player.attack / 2), inform_Player.attack)
    if pAttack == inform_Player.attack / 2:
        print("You miss!!")
    else:
        mSWarrior.hp -= pAttack
        print("You deal: {} damage!!".format(pAttack))

    option = input("")
    if mSWarrior.hp <= 0:
        mSWarrior.hp = 0
        win()
    elif mSWarrior2.hp <= 0:
        mSWarrior2.hp = 0
        win()
    elif mSWarrior3.hp <= 0:
        mSWarrior3.hp = 0
        win()
    elif mSWarrior4.hp <= 0:
        mSWarrior4.hp = 0
        win()
    clear()
    if eAttack == eAttack_1 == eAttack_2 == eAttack_3 == eAttack_boss == 0:
        print("Enemy Miss")
    else:
        if eAttack:
            inform_Player.hp -= eAttack
            print("The enemy deals {} damage!!".format(eAttack))
        elif eAttack_1:
            inform_Player.hp -= eAttack_1
            print("The enemy deals {} damage!!".format(eAttack_1))
        elif eAttack_2:
            inform_Player.hp -= eAttack_2
            print("The enemy deals {} damage!!".format(eAttack_2))
        elif eAttack_3:
            inform_Player.hp -= eAttack_3
            print("The enemy deals {} damage!!".format(eAttack_3))
        elif eAttack_boss:
            inform_Player.hp -= eAttack_boss
            print("The enemy deals {} damage!!".format(eAttack_boss))

    option = input("> ")
    if inform_Player.hp == 0:
        die()

    else:
        player_fight(option.lower())


def hill():
    clear()

    inform_Player.hp += 50
    if inform_Player.hp > inform_Player.max_hp:
        inform_Player.max_hp = inform_Player.hp
        print("Your current {}".format(inform_Player.hp))
    print("You drink an Energy-Drink")
    option = input("> ")
    prompt()


"""
    Player Selection
"""


# Game Interactivity
def print_location():
    if inform_Player.location == '':
        print(inform_Player.location)
        print("OK")
        print("I'm sorry I think you cannot go there")
        print("Please select again your location")
        print("Now the location goes to the 'START'.")
        inform_Player.location = '5,1'
        print(inform_Player.location)
        prompt()
    else:
        print("\n" + ('#' * (4 + len(inform_Player.location))))
        print("# " + inform_Player.location.upper() + ' #')
        print('# ' + char_pos.map_zone[inform_Player.location]['DESCRIPTION'] + ' #')
        print("\n" + ('#' * (4 + len(inform_Player.location))))


def player_move(action):
    ask = "Where would you like to move to? \n"
    print("1. move north\n2. move south\n3. move west\n4. move east\n")
    destination_input = input(ask)

    if destination_input in ['move north']:
        move_position.BLOCKED = False
        destination = char_pos.map_zone[inform_Player.location][char_pos.UP]
        movement_handler(destination)
    elif destination_input in ['move south']:
        destination = char_pos.map_zone[inform_Player.location][char_pos.DOWN]
        movement_handler(destination)
    elif destination_input in ['move west']:
        destination = char_pos.map_zone[inform_Player.location][char_pos.LEFT]
        movement_handler(destination)
    elif destination_input in ['move east']:
        destination = char_pos.map_zone[inform_Player.location][char_pos.RIGHT]
        movement_handler(destination)


def prefight():
    global enemy
    enemyNum = mswarrior
    if enemyNum == 1:
        enemy = mswarrior[1]
    elif enemyNum == 2:
        enemy = mswarrior[2]
    elif enemyNum == 3:
        enemy = mswarrior[3]
    elif enemyNum == 4:
        enemy = mswarrior[4]


def player_fight(action):
    print("{0} vs {1}".format(inform_Player.name, mSWarrior.name))
    print("{0}'s Hp: {1}/{2} - {3}'s Hp: {4}/{5}".format(
        inform_Player.name, inform_Player.hp, inform_Player.max_hp,
        mSWarrior.name, mSWarrior.hp, mSWarrior.max_hp
    ))

    print("Please select your action. (name of action)")
    print("1. fight\n2. run\n3. use potion\n")
    action = input("> ")

    if(action == "1") or (action == "fight"):
        attack()
    elif(action == "2") or (action == "run"):
        prompt()
    elif (action == "3") or (action == "use potion"):
        hill()
    else:
        fight()


def player_fight2(action):
    print("{0} vs {1}".format(inform_Player.name, mSWarrior2.name))
    print("{0}'s Hp: {1}/{2} - {3}'s Hp: {4}/{5}".format(
        inform_Player.name, inform_Player.hp, inform_Player.max_hp,
        mSWarrior2.name, mSWarrior2.hp, mSWarrior2.max_hp
    ))

    print("Please select your action. (name of action)")
    print("1. fight\n2. run\n3. use potion\n")
    action = input("> ")

    if (action == "1") or (action == "fight"):
        attack()
    elif (action == "2") or (action == "run"):
        prompt()
    elif (action == "3") or (action == "use potion"):
        hill()
    else:
        fight()


def player_fight3(action):
    print("{0} vs {1}".format(inform_Player.name, mSWarrior3.name))
    print("{0}'s Hp: {1}/{2} - {3}'s Hp: {4}/{5}".format(
        inform_Player.name, inform_Player.hp, inform_Player.max_hp,
        mSWarrior3.name, mSWarrior3.hp, mSWarrior3.max_hp
    ))

    print("Please select your action. (name of action)")
    print("1. fight\n2. run\n3. use potion\n")
    action = input("> ")

    if (action == "1") or (action == "fight"):
        attack()
    elif (action == "2") or (action == "run"):
        prompt()
    elif (action == "3") or (action == "use potion"):
        hill()
    else:
        fight()


def player_fight4(action):
    print("{0} vs {1}".format(inform_Player.name, mSWarrior4.name))
    print("{0}'s Hp: {1}/{2} - {3}'s Hp: {4}/{5}".format(
        inform_Player.name, inform_Player.hp, inform_Player.max_hp,
        mSWarrior4.name, mSWarrior4.hp, mSWarrior4.max_hp
    ))

    print("Please select your action. (name of action)")
    print("1. fight\n2. run\n3. use potion\n")
    action = input("> ")

    if (action == "1") or (action == "fight"):
        attack()
    elif (action == "2") or (action == "run"):
        prompt()
    elif (action == "3") or (action == "use potion"):
        hill()
    else:
        fight()


def player_fight5(action):
    print("{0} vs {1}".format(inform_Player.name, mSKing.name))
    print("{0}'s Hp: {1}/{2} - {3}'s Hp: {4}/{5}".format(
        inform_Player.name, inform_Player.hp, inform_Player.max_hp,
        mSKing.name, mSKing.hp, mSKing.max_hp
    ))

    print("Please select your action. (name of action)")
    print("1. fight\n2. run\n3. use potion\n")
    action = input("> ")

    if (action == "1") or (action == "fight"):
        attack()
    elif (action == "2") or (action == "run"):
        prompt()
    elif (action == "3") or (action == "use potion"):
        hill()
    else:
        fight()


"""
    Selecting Menu-Area.
    It begins from 'start_menu()' function 
    which is located in the below of this entire codes.
"""


def prompt():
    """
        Could not move this codes below because it doesn't change 'inform_Player' that form set as 'global'
        should be located in the same file.
    """
    if (inform_Player.location == mSWarrior.location) or (inform_Player.location == mSKing.location) \
            or (inform_Player.location == mSWarrior2.location) or (inform_Player.location == mSWarrior3.location) \
            or (inform_Player.location == mSWarrior4.location):
        print("\n" + "====================")
        print("¡¡¡¡ Warning  Warning  Warning !!!!")
        print("Your current location is : {0}".format(inform_Player.location))
        print("Your current HP and AP is : {0} {1}".format(inform_Player.hp, inform_Player.ap))
        print("Please select your action. (name of action)")
        print("1. run\n2. fight\n3. use_potion\nq. quit\n")
        action = input("> ")
        works_action = [
            'run', 'fight', 'use_potion',
            '1', '2', '3', 'q'
        ]

        while action.lower() not in works_action:
            print("Unknown action.. Please try again!!\n")
            action = input("> ")
        if action.lower() is 'q':
            exit(0)
        elif action.lower() in ['run', '1']:
            player_move(action.lower())
        elif action.lower() in ['fight', '2']:
            if inform_Player.location == mSWarrior.location:
                player_fight(action.lower())
            elif inform_Player.location == mSWarrior2.location:
                player_fight2(action.lower())
            elif inform_Player.location == mSWarrior3.location:
                player_fight3(action.lower())
            elif inform_Player.location == mSWarrior4.location:
                player_fight4(action.lower())
            elif inform_Player.location == mSKing.location:
                player_fight5(action.lower())
        elif action.lower() in ['use_potion', '3']:
            hill()

    elif (inform_Player.location != mSWarrior.location) and (inform_Player.location != mSKing.location) \
            and (inform_Player.location != mSWarrior2.location) and (inform_Player.location != mSWarrior3.location) \
            and (inform_Player.location != mSWarrior4.location) and (inform_Player.location != '2,1')\
            and (inform_Player.location != '3,1') and (inform_Player.location != '3,4') \
            and (inform_Player.location != '4,6') and (inform_Player.location != '1,4')\
            and (inform_Player.location != '1,7'):

        # if(mSWarrior.hp <= 0) or (mSWarrior2.hp <= 0) or (mSWarrior3.hp <= 0) or (mSWarrior4.hp <= 0):
        print("\n" + "====================")
        print("Your current location is : {0}".format(inform_Player.location))
        print("Your current HP and AP is : {0} {1}".format(inform_Player.hp, inform_Player.ap))
        print("Please select your action. (name of action)")
        print("1. move\n2. q")
        action = input("> ")
        works_action = [
            'move',
            '1', 'q'
        ]

        while action.lower() not in works_action:
            print("Unknown action.. Please try again!!\n")
            action = input("> ")
        if action.lower() is 'q':
            exit(0)
        elif action.lower() in ['move', '1']:
            player_move(action.lower())

    # When User arrived at 'IRON SHIELD'
    elif inform_Player.location == "2,1":
        # Store function for IRON_SHIELD
        print("\n" + "====================")
        print("Congratulations!! Now you've got the {} !!!".format(inform_Player.current_shield))
        print("Your current location is : {0}".format(inform_Player.location))
        print("Your current HP and AP is : {0} {1}".format(inform_Player.hp, inform_Player.ap))
        print("Please select your action. (name of action)")
        print("1. move\n2. q")
        action = input("> ")
        works_action = [
            'move', 'q',
            '1', '2',
        ]

        while action.lower() not in works_action:
            print("Unknown action.. Please try again!!\n")
            action = input("> ")
        if action.lower() is 'q':
            exit(0)
        elif action.lower() in ['move', '1']:
            player_move(action.lower())

    # When User arrived at 'IRON SWORD'
    elif inform_Player.location == "4,6":
        # Store function for IRON_SWORD
        print("\n" + "====================")
        print("Congratulations!! Now you've got the speed -> {} !!!".format(inform_Player.speed))
        print("Your current location is : {0}".format(inform_Player.location))
        print("Your current HP and AP is : {0} {1}".format(inform_Player.hp, inform_Player.ap))
        print("Please select your action. (name of action)")
        print("1. move\n2. q")
        action = input("> ")
        works_action = [
            'move', 'q',
            '1', '2'
        ]

        while action.lower() not in works_action:
            print("Unknown action.. Please try again!!\n")
            action = input("> ")
        if action.lower() is 'q':
            exit(0)
        elif action.lower() in ['move', '1']:
            player_move(action.lower())

    # When User arrived HEALTH POSITION
    elif (inform_Player.location == "3,1") or (inform_Player.location == "3,4"):
        print("\n" + "====================")
        print("Your current location is : {0}".format(inform_Player.location))
        print("Your current HP and AP is : {0} {1}".format(inform_Player.hp, inform_Player.ap))
        print("Do you want to hill your HP?")
        print("Please select your action. (name of action)")

        print("1. move\n2. hill\n3. q")
        action = input("> ")
        works_action = [
            'move', 'hill', 'q',
            '1', '2'
        ]

        while action.lower() not in works_action:
            print("Unknown action.. Please try again!!\n")
            action = input("> ")
        if action.lower() is 'q':
            exit(0)
        elif action.lower() in ['move', '1']:
            player_move(action.lower())
        elif action.lower() in ['hill', '2']:
            hill()

    # When user reacted to the 'BOSS KEY'
    elif inform_Player.location == '1,4':
        print("\n" + "====================")
        print("Congratulations!! Now you've got the {} !!!".format(inform_Player.boss_key))
        print("Your current location is : {0}".format(inform_Player.location))
        print("Your current HP and AP is : {0} {1}".format(inform_Player.hp, inform_Player.ap))
        print("Please select your action. (name of action)")
        print("1. move\n2. q")
        action = input("> ")
        works_action = [
            'move', 'q',
            '1', '2'
        ]

        while action.lower() not in works_action:
            print("Unknown action.. Please try again!!\n")
            action = input("> ")
        if action.lower() is 'q':
            exit(0)
        elif action.lower() in ['move', '1']:
            player_move(action.lower())

    # When user reacted to the 'END'.
    elif inform_Player.location == '1,7':
        # If player has the key to way out, the he can goes out.
        print("Congratulation!! Finally you've finished the game!!!!")
        print("Do you want to continue this game?\n1. Continue\nq. quit")
        continue_input = input("?> ")
        if continue_input == '1':
            print("Start this game again..")
            start_menu()
        else:
            print("Terminate this game..")
            sys.exit()
    else:
        print("The place where you input has no spaces. Please input again.")
        action = input("> ")
        while action.lower() not in action:
            print("Unknown action.. Please try again!!\n")
            action = input("> ")
        if action.lower() is 'q':
            exit(0)
        elif action.lower() in ['move', '1']:
            player_move(action.lower())


# Make sure that there should be no `unnecessary text`.
# because when we go to 'prompt()' function directly, You can see some text that it's `unnecessary text`.
# To deal with this problem, makes the function that doesn't view `unnecessary text`.
def pre_prompt():
    print("Wait a second..")
    time.sleep(0.1)
    prompt()


# Naming User-Name and applies to
def user_information():
    # Collect names
    name_input = "Please Enter your Name!!\n"
    for player_char in name_input:
        sys.stdout.write(player_char)
        sys.stdout.flush()
        time.sleep(0.05)

    player_name = input("> ")

    print("Hold on please..")
    time.sleep(1)

    # Collect jobs
    job_input = "Please Enter your Job!!\n"
    job_information = "1. warrior"
    # job_information = "1. warrior  2. magician  3. troll"
    for character in job_input:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.05)

    for character in job_information:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.02)

    player_job = input("> ")
    valid_jobs = ["warrior"]
    # valid_jobs = ["warrior", "magician", "troll"]

    """
        This 'global inform_Player can use in the 'same_file' only.
    """
    global inform_Player
    inform_Player = Player(player_name, player_job)

    while player_job.lower() not in valid_jobs:
        player_job = input("Please input again> ")

    if player_job.lower() in valid_jobs:
        inform_Player.job = player_job
        print("Great " + player_name + "!!!, Confirmed that you selected " + player_job + " in this game~~!!.\n")

    if inform_Player.job is "warrior":
        print("Alright!!")

    # if inform_Player.job is "magician":
    #     inform_Player.hp = 60
    #     inform_Player.ap = 15
    #     inform_Player.defence = 3
    #     inform_Player.speed = 5
    #
    # if inform_Player.job is "troll":
    #     inform_Player.hp = 60
    #     inform_Player.ap = 60
    #     inform_Player.defence = 1
    #     inform_Player.speed = 1

    confirm_information = 'Welcome to ' + name_input + ' the' + player_job + "!\n"
    for character_information in confirm_information:
        sys.stdout.write(character_information)
        sys.stdout.flush()

        pre_prompt()


def setup_game():
    # Clear Screen.
    clear()

    # Input User-Name and User-Job, and get the information of it.
    user_information()


def main_menu():
    print("##############")
    print("Welcome to Xeno-Adventure")
    print("There is a selection what you want to do!!!")
    print("1. Start\n2. Credits\nq. Stop the game")

    main_menu_selection = input("Please select 'any-menu' you want!!!")
    main_menu_action = [
        'Start', 'Credits', 'Stop the game', '1', '2', 'q',
        'start', 'credits', 'stop the game', 'Q'
    ]

    while main_menu_selection.lower() in ['q', 'Q']:
        print("Unknown selection... Please Try again!!\n")
        main_menu_selection = input(">")

    if main_menu_selection.lower() is 'q':
        sys.exit()
    elif main_menu_selection.lower() in ['Start', 'start', '1']:
        setup_game()
    elif main_menu_selection.lower() in ['Credit', 'credit', '2']:
        from Credits.Credits import Credit
        Credit()


# Starting from here.
def start_menu():
    print("##############")
    print("Wait for a second..")

    start_game = input("If you want to start this game, \n"
                       "Press 'any-key' to continue!!!")

    print(start_game)
    main_menu()


if __name__ == "__main__":
    start_menu()
