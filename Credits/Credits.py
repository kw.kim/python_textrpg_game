import os

from game import *


creator_name = "Geonwoo-Kim"
director_name = "Mr.Gaetan-Guerrero"
director_name2 = "Ms.Mael-Le-Goff"


def Credit():
    clear()
    print("----------Credit-----------")
    print("Creator: {0}".format(creator_name))
    print("Director: {0}\n {1}".format(director_name, director_name2))
    credit_selection()


def credit_selection():
    back_menu = input("Do you want to go back to Main Directory?\n"
                      "If you want, Please press 'y' key.\n"
                      "Otherwise, Please press 'n' key.")
    back_menu_list = []

    while 1:
        if back_menu is 'y':
            main = main_menu()
            back_menu_list.append(main)
            break

        elif back_menu is 'n':
            print("Terminating Games..")
            exit(0)
            break

        else:
            print("Please press 'y' key or 'n' key.")
            time.sleep(2)
            Credit()

