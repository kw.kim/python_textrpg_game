# Python Text RPG
# Created by V Savard, inspired by Bryan Tong's Tutorial

# import cmd
# import textwrap
import sys
import os
import time

# import random

screen_width = 100


#### Player Setup ####
class Player:
    def __init__(self):
        self.name = ''
        self.hp = 0
        self.charisma = 0
        self.job = ''
        self.status_effects = []
        self.location = '5,1'
        self.game_over = False


myPlayer = Player()


#### Title Screen ####

def title_screen_selections():
    option = input("> ")
    if option.lower() == "play":
        setup_game()
    elif option.lower() == "help":
        help_menu()
    elif option.lower() == "quit":
        sys.exit()
    while option.lower() not in ['play', 'help', 'quit']:
        print("Please enter a valid command (-play -help -quit).")
        option = input("> ")
        if option.lower() == "play":
            setup_game()
        elif option.lower() == "help":
            help_menu()
        elif option.lower() == "quit":
            sys.exit()


def title_screen():
    os.system('cls')
    print('###############################################')
    print('#         Welcome to this Text RPG!           #')
    print('###############################################')
    print(' > Play                                        ')
    print(' > Help                                        ')
    print(' > Quit                                        ')
    print(' Have fun.                                     ')
    title_screen_selections()


def help_menu():
    print('###############################################')
    print('#         Welcome to this Text RPG!           #')
    print('###############################################')
    print('- Use up, down, left and right to move.        ')
    print('- Type your commands                           ')
    print('- Use "look" to inspect something              ')
    print('- Email me if there is a bug.                  ')
    title_screen_selections()

    #### Game functionality ####

    #### map ####
    """
    a1 a2...
    -------------
    |  |  |  |  | a4
    -------------
    |  |  |  |  | b4 ...
    -------------
    |  |  |  |  |
    -------------
    |  |  |  |  |
    -------------
    """


ZONENAME = ''
DESCRIPTION = 'description'
EXAMINATION = 'examination'
SOLVED = False
BLOCKED = False
UP = 'up', 'north'
DOWN = 'down', 'south'
LEFT = 'left', 'west'
RIGHT = 'right', 'east'

solved_places = {
    '1,1': False, '1,2': False, '1,3': False, '1,4': False, '1,5': False, '1,6': False, '1,7': False,
    '2,1': False, '2,2': False, '2,3': False, '2,4': False, '2,5': False, '2,6': False, '2,7': False,
    '3,1': False, '3,2': False, '3,3': False, '3,4': False, '3,5': False, '3,6': False, '3,7': False,
    '4,1': False, '4,2': False, '4,3': False, '4,4': False, '4,5': False, '4,6': False, '4,7': False,
    '5,1': False, '5,2': False, '5,3': False, '5,4': False, '5,5': False, '5,6': False, '5,7': False,
}

map_zone = {
    '1,1': {
        'ZONENAME': '11',
        'DESCRIPTION': "This is the town's entrance",
        'EXAMINATION': 'examination',
        'SOLVED': False,
        'BLOCKED': True,
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
    },
    '1,2': {
        'ZONENAME': '12',
        'DESCRIPTION': 'description',
        'EXAMINATION': 'examination',
        'SOLVED': False,
        'BLOCKED': True,
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
    },
    '1,3': {
        'ZONENAME': 'SKELETON_WARRIOR_2',
        'DESCRIPTION': 'description',
        'EXAMINATION': 'examination',
        'SOLVED': False,
        'BLOCKED': False,
        UP: '',
        DOWN: '2,3',
        LEFT: '',
        RIGHT: '1,4'
    },
    '1,4': {
        'ZONENAME': 'BOSS_KEY',
        'DESCRIPTION': 'description',
        'EXAMINATION': 'examination',
        'SOLVED': False,
        'BLOCKED': False,
        UP: '',
        DOWN: '',
        LEFT: '1,3',
        RIGHT: ''
    },

    '1,5': {
        'ZONENAME': '15',
        'DESCRIPTION': 'description',
        'EXAMINATION': 'examination',
        'SOLVED': False,
        'BLOCKED': True,
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: ''
    },
    '1,6': {
        'ZONENAME': '16',
        'DESCRIPTION': 'description',
        'EXAMINATION': 'examination',
        'SOLVED': False,
        'BLOCKED': True,
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: ''
    },
    '1,7': {
        'ZONENAME': 'end',
        'DESCRIPTION': 'description',
        'EXAMINATION': 'examination',
        'SOLVED': False,
        'BLOCKED': True,
        UP: '',
        DOWN: '2,7',
        LEFT: '',
        RIGHT: ''
    },
    '2,1': {
        'ZONENAME': '21',
        'DESCRIPTION': 'description',
        'EXAMINATION': 'examination',
        'SOLVED': False,
        'BLOCKED': True,
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: ''
    },
    '2,2': {
        'ZONENAME': "22",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True,
    },
    '2,3': {
        'ZONENAME': "23",
        'DESCRIPTION': "Hallway",
        'EXAMINATION': "example",
        UP: '1,3',
        DOWN: '3,3',
        LEFT: '',
        RIGHT: '',
        'SOLVED': True,
        'BLOCKED': False,
    },
    '2,4': {
        'ZONENAME': "24",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True,
    },
    '2,5': {
        'ZONENAME': "25",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True,
    },
    '2,6': {
        'ZONENAME': "26",
        'DESCRIPTION': "Hallway",
        'EXAMINATION': "example",
        UP: '',
        DOWN: '3,6',
        LEFT: '',
        RIGHT: '2,7',
        'SOLVED': True,
        'BLOCKED': False,
    },
    '2,7': {
        'ZONENAME': "SKELETON_KING",
        'DESCRIPTION': "",
        'EXAMINATION': "example",
        UP: '1,7',
        DOWN: '',
        LEFT: '2,6',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': False,
    },
    '3,1': {
        'ZONENAME': "HEALTH_POTION",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '2,1',
        DOWN: '4,1',
        LEFT: '',
        RIGHT: '',
        'SOLVED': True,
        'BLOCKED': False,
    },
    '3,2': {
        'ZONENAME': "",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True,
    },
    '3,3': {
        'ZONENAME': "33",
        'DESCRIPTION': "Hallway",
        'EXAMINATION': "example",
        UP: '2,3',
        DOWN: '4,3',
        LEFT: '',
        RIGHT: '3,4',
        'SOLVED': True,
        'BLOCKED': False,
    },
    '3,4': {
        'ZONENAME': "HEALTH_POTION",
        'DESCRIPTION': "",
        'EXAMINATION': "example",
        UP: '',
        DOWN: '',
        LEFT: '3,3',
        RIGHT: '3,5',
        'SOLVED': False,
        'BLOCKED': False,
    },
    '3,5': {
        'ZONENAME': "SKELETON_WARRIOR_4",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '3,4',
        RIGHT: '3,6',
        'SOLVED': False,
        'BLOCKED': False,
    },
    '3,6': {
        'ZONENAME': "Hallway",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '3,6',
        DOWN: '4,6',
        LEFT: '3,5',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': False,
    },
    '3,7': {
        'ZONENAME': "",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True,
    },
    '4,1': {
        'ZONENAME': "Hallway",
        'DESCRIPTION': "description",
        'EXAMINATION': "example",
        UP: '3,1',
        DOWN: '5,1',
        LEFT: '',
        RIGHT: '4,2',
        'SOLVED': True,
        'BLOCKED': False,
    },
    '4,2': {
        'ZONENAME': "SKELETON_WARRIOR_1",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '4,1',
        RIGHT: '4,3',
        'SOLVED': False,
        'BLOCKED': False,
    },
    '4,3': {
        'ZONENAME': "Hallway",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '3,3',
        DOWN: '5,3',
        LEFT: '4,2',
        RIGHT: '',
        'SOLVED': True,
        'BLOCKED': False,
    },
    '4,4': {
        'ZONENAME': "",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True
    },
    '4,5': {
        'ZONENAME': "",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True
    },
    '4,6': {
        'ZONENAME': "Hallways",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '3,6',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True
    },
    '4,7': {
        'ZONENAME': "",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True
    },
    '5,1': {
        'ZONENAME': "START",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '4,1',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': True,
        'BLOCKED': False
    },
    '5,2': {
        'ZONENAME': "",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True
    },
    '5,3': {
        'ZONENAME': "SKELETON_WARRIOR_3",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '4,3',
        DOWN: '',
        LEFT: '',
        RIGHT: '5,4',
        'SOLVED': False,
        'BLOCKED': False,
    },
    '5,4': {
        'ZONENAME': "IRON_SWORD",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '5,3',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': False,
    },
    '5,5': {
        'ZONENAME': "",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True
    },
    '5,6': {
        'ZONENAME': "",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True
    },
    '5,7': {
        'ZONENAME': "",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        'BLOCKED': True
    }

}

# ####
# Game interactivity
# ####


def print_location():
    print('\n' + ('#' * (4 + (len(myPlayer.location)))))
    print('# ' + myPlayer.location + ' #')
    print('# ' + map_zone[myPlayer.location]['DESCRIPTION'] + ' #')
    print('\n' + ('#' * (4 + (len(myPlayer.location)))))


def prompt():
    print('\n' + '=========================')
    print('what would you like to do?')
    action = input("> ")
    acceptable_actions = ['move', 'go', 'travel',
                          'walk', 'quit', 'examine',
                          'interact', 'look', 'inspect']
    while action.lower() not in acceptable_actions:
        print("Unknown action, try again!")
        action = input("> ")
    if action.lower() == 'quit':
        sys.exit()
    elif action.lower() in ['move', 'go', 'travel', 'walk']:
        player_move(action.lower())
    elif action.lower() in ['examine', 'interact', 'look', 'inspect']:
        player_examine(action.lower())


def player_move(action):
    ask = 'Where do you want to go exactly?\n'
    dest = input(ask)

    if dest in ['up', 'north']:
        destination = map_zone[myPlayer.location][UP]
        mouvement_handler(destination)
    elif dest in ['left', 'west']:
        destination = map_zone[myPlayer.location][LEFT]
        mouvement_handler(destination)
    elif dest in ['right', 'east']:
        destination = map_zone[myPlayer.location][RIGHT]
        mouvement_handler(destination)
    elif dest in ['down', 'south']:
        destination = map_zone[myPlayer.location][DOWN]
        mouvement_handler(destination)


def mouvement_handler(destination):
    print('\n' + 'You have arrived to your destination (' + destination + ').')
    myPlayer.location = destination
    print_location()


def player_examine(action):
    if map_zone[myPlayer.location]['SOLVED']:
        print('You have already been to this zone.')
    else:
        print('You can trigger a puzzle here.')


#### Game functionality ####

def main_game_loop():
    while myPlayer.game_over is False:
        prompt()
    # here handle if puzzle have been solved (full completion)


def setup_game():
    os.system('cls')

    question1 = 'Hello, what is your name?\n'
    for character in question1:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.05)
    player_name = input("> ")
    myPlayer.name = player_name

    question2 = 'What is your title/job?\n'
    question2added = 'You can be a cook, a barman or barmaid, or a singer.\n'
    for character in question2:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.05)
    for character in question2added:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.02)
    player_job = input("> ")
    valid_jobs = ['cook', 'barman', 'barmaid', 'singer']
    while player_job.lower() not in valid_jobs:
        player_job = input("> ")
    if player_job.lower() in valid_jobs:
        myPlayer.job = player_job
        print('Good. We need a decent ' + player_job + ' in our pub.\n')

    ### Player Stats ###
    if myPlayer.job is 'cook':
        self.hp = 100
        self.charisma = 40
    if myPlayer.job is 'barman':
        self.hp = 70
        self.charisma = 70
    if myPlayer.job is 'barmaid':
        self.hp = 60
        self.charisma = 80
    if myPlayer.job is 'singer':
        self.hp = 45
        self.charisma = 95

    ### Introduction ###
    question3 = 'Welcome ' + player_name + ' the ' + player_job + '!\n'
    for character in question3:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.05)
    player_name = input("> ")
    myPlayer.name = player_name

    os.system('cls')
    speech1 = 'Welcome to our humble pub.\n'
    speech2 = 'We know you are new in this area.\n'
    speech3 = 'Make a name for your self here and beyond!\n'
    speech4 = 'Good luck, you will need it.\n'
    for character in speech1:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.05)
    for character in speech2:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.05)
    for character in speech3:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.1)
    for character in speech4:
        sys.stdout.write(character)
        sys.stdout.flush()
        time.sleep(0.3)
    os.system('cls')
    print('####################################')
    print('##   The adventure starts now!    ##')
    print('####################################')
    main_game_loop()


title_screen()
