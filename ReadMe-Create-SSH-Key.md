# How to create a `SSH-KEY`

This project template based on [`SSH-KEY`] Project.

## Procedure of create `SSH-KEY`

### Creating `SSH-KEY`

Type 
```
ssh-keygen -t rsa -C "kw.kim@xenoimpact.com"
```

The `ssh-key` generate in your file-directory,
for the example, this computer set into

```
C:\Users\xeno/.ssh/id_rsa.
```
 
But it would be changed as

```
C:\Users\Adminstrator/.ssh/id_rsa
```

### Go to `cd ~/.ssh`

Open `git bash` and

Go to 
```
cd ~/.ssh
```

then
```
cat ~/.ssh/id_rsa.pub | clip
```

You can see that it's already generated
into your `/.ssh` file.

```
You can proceed on `git bash`
this all of things.
```

### Paste to the `SSH`.

Paste the key in GitLab: Settings > SSH Key



### Further information

------------
# add SSH-Key to gitLab
- Windows :
    Create a public/private pair of key : ssh-keygen -t rsa -C "your_email@example.com"
    The new key is in the .ssh folder, to go to the folder : cd ~/.ssh
    Copy the public key : clip < ~/.ssh/id_rsa.pub (here the name of the key is id_rsa.pub - default name)
    Paste the key in GitLab: Settings > SSH Key
    
 cd existing_folder
git init
git remote add origin git@gitlab.com:xn-gaetan/test2.git
git add .
git commit -m "Initial commit"
git push -u origin master


https://docs.gitlab.com/ee/ssh/README.html#rsa-ssh-keys
https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#command-shell
https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair
https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html
