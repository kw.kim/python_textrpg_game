import os
import sys
import time

from User_Position.user_position import *

screen_width = 100

char_pos = move_position()

"""
    Could not separate 'Player' Class
    because it has initializer-Function.

    Set the default-job as 'warrior'.
"""

ZONENAME = ''
DESCRIPTION = 'description'
EXAMINATION = 'examination'

# When you solved mission, it'll changes 'True'.
SOLVED = False
UP = 'up', 'north',
DOWN = 'down', 'south',
LEFT = 'left', 'west',
RIGHT = 'right', 'east'

# User cannot go there because there are no space
# that User go.
BLOCKED = False
x = 7
y = 5
a = [[0] * x] * y
a[0][0] = False

x_rows = [1, 2, 3, 4, 5, 6, 7]
y_rows = [1, 2, 3, 4, 5]
print(a[0][0])

player_pos = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
    31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50
]
# print(player_pos)

solved_places = {
    '1,1': False, '1,2': False, '1,3': False, '1,4': False, '1,5': False, '1,6': False, '1,7': False,
    '2,1': False, '2,2': False, '2,3': False, '2,4': False, '2,5': False, '2,6': False, '2,7': False,
    '3,1': False, '3,2': False, '3,3': False, '3,4': False, '3,5': False, '3,6': False, '3,7': False,
    'c41': False, '4,2': False, '4,3': False, '4,4': False, '4,5': False, '4,6': False, '4,7': False,
    'c51': False, '5,2': False, '5,3': False, '5,4': False, '5,5': False, '5,6': False, '5,7': False,
}

map_zone = {
    '1,1': {
        'ZONENAME': "",
        'DESCRIPTION': "description",
        'EXAMINATION': "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        'SOLVED': False,
        BLOCKED: True,
    },
    '1,2': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True,
    },
    '1,3': {
        'ZONENAME': "SKELETON_WARRIOR_2",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '2,3',
        LEFT: '',
        RIGHT: '1,4',
        SOLVED: False,
        BLOCKED: False,
    },
    '1,4': {
        'ZONENAME': "BOSS_KEY",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: False,
    },
    '1,5': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True,
    },
    '1,6': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True,
    },
    '1,7': {
        'ZONENAME': "END",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '2,7',
        LEFT: '',
        RIGHT: '',
        SOLVED: True,
        BLOCKED: False,
    },
    '2,1': {
        'ZONENAME': "IRON_SHIELD",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '3,1',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: False,
    },
    '2,2': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True,
    },
    '2,3': {
        'ZONENAME': "23",
        DESCRIPTION: "Hallway",
        EXAMINATION: "example",
        UP: '1,3',
        DOWN: '3,3',
        LEFT: '',
        RIGHT: '',
        SOLVED: True,
        BLOCKED: False,
    },
    '2,4': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True,
    },
    '2,5': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True,
    },
    '2,6': {
        'ZONENAME': "26",
        DESCRIPTION: "Hallway",
        EXAMINATION: "example",
        UP: '',
        DOWN: '3,6',
        LEFT: '',
        RIGHT: '2,7',
        SOLVED: True,
        BLOCKED: False,
    },
    '2,7': {
        'ZONENAME': "SKELETON_KING",
        DESCRIPTION: "",
        EXAMINATION: "example",
        UP: '1,7',
        DOWN: '',
        LEFT: '2,6',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: False,
    },
    '3,1': {
        'ZONENAME': "HEALTH_POTION",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '2,1',
        DOWN: '4,1',
        LEFT: '',
        RIGHT: '',
        SOLVED: True,
        BLOCKED: False,
    },
    '3,2': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True,
    },
    '3,3': {
        'ZONENAME': "33",
        DESCRIPTION: "Hallway",
        EXAMINATION: "example",
        UP: '2,3',
        DOWN: '4,3',
        LEFT: '',
        RIGHT: '3,4',
        SOLVED: True,
        BLOCKED: False,
    },
    '3,4': {
        'ZONENAME': "HEALTH_POTION",
        DESCRIPTION: "",
        EXAMINATION: "example",
        UP: '',
        DOWN: '',
        LEFT: '3,3',
        RIGHT: '3,5',
        SOLVED: False,
        BLOCKED: False,
    },
    '3,5': {
        'ZONENAME': "SKELETON_WARRIOR_4",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '3,4',
        RIGHT: '3,6',
        SOLVED: False,
        BLOCKED: False,
    },
    '3,6': {
        'ZONENAME': "Hallway",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '3,6',
        DOWN: '4,6',
        LEFT: '3,5',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: False,
    },
    '3,7': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True,
    },
    'c41': {
        'ZONENAME': "Hallway",
        DESCRIPTION: "description",
        EXAMINATION: "example",
        UP: '3,1',
        DOWN: '5,1',
        LEFT: '',
        RIGHT: '4,2',
        SOLVED: True,
        BLOCKED: False,
    },
    '4,2': {
        'ZONENAME': "SKELETON_WARRIOR_1",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '4,1',
        RIGHT: '4,3',
        SOLVED: False,
        BLOCKED: False,
    },
    '4,3': {
        'ZONENAME': "Hallway",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '3,3',
        DOWN: '5,3',
        LEFT: '4,2',
        RIGHT: '',
        SOLVED: True,
        BLOCKED: False,
    },
    '4,4': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True
    },
    '4,5': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True
    },
    '4,6': {
        'ZONENAME': "Hallways",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '3,6',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True
    },
    '4,7': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True
    },
    'c51': {
        'ZONENAME': "START",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: 'c41',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: True,
        BLOCKED: False
    },
    '5,2': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True
    },
    '5,3': {
        'ZONENAME': "SKELETON_WARRIOR_3",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '4,3',
        DOWN: '',
        LEFT: '',
        RIGHT: '5, 4',
        SOLVED: False,
        BLOCKED: False,
    },
    '5,4': {
        'ZONENAME': "IRON_SWORD",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '5,3',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: False,
    },
    '5,5': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True
    },
    '5,6': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True
    },
    '5,7': {
        'ZONENAME': "",
        DESCRIPTION: "description",
        EXAMINATION: "examine",
        UP: '',
        DOWN: '',
        LEFT: '',
        RIGHT: '',
        SOLVED: False,
        BLOCKED: True
    }
}


class Player:
    def __init__(self):
        self.name = ""
        self.job = "warrior"
        self.hp = "60"
        self.ap = "10"
        self.defence = "5"
        self.speed = "10"
        self.status_effects = []
        self.location = '5,1'
        self.game_over = False


class Skeleton_Warrior:
    def __init__(self):
        self.name = ""
        self.hp = "25"
        self.ap = "10"
        self.defence = "0"
        self.speed = "5"
        self.location = "Skeleton_Warrior_"


class Skeleton_King:
    def __init__(self):
        self.name = ""
        self.hp = "40"
        self.ap = "12"
        self.defence = "5"
        self.speed = "15"


mPlayer = Player()
mSWarrior = Skeleton_Warrior()
mSKing = Skeleton_King()


# Generated 'clear' function to clear current Screen.
def clear():
    # os.system('clear')
    print("\n" * 30)


# Game Interactivity
def print_location():
    print("\n" + ('#' * (4 + len(mPlayer.location))))
    print("# " + mPlayer.location.upper() + ' #')
    print('# ' + map_zone[mPlayer.location]['DESCRIPTION'] + ' #')
    print("\n" + ('#' * (4 + len(mPlayer.location))))


def prompt():
    print("\n" + "====================")
    # print("Your current location is : {0}".format(mPlayer.location))
    # print("Your current HP and AP is : {0} {1}".format(mPlayer.hp, mPlayer.ap))
    print("Please select your action. (name of action)")
    print("1. move\n2. run\n3. fight\n4. use_key\n5. use_potion\n6. examine")
    action = input("> ")
    works_action = [
        'move', 'run', 'fight', 'use_key', 'use_potion', 'examine',
        '1', '2', '3', '4', '5', '6'
    ]

    while action.lower() not in works_action:
        print("Unknown action.. Please try again!!\n")
        action = input("> ")
    if action.lower() == 'q':
        exit(0)
    elif action.lower() in ['move', 'run', '1', '2']:
        player_move(action.lower())
    elif action.lower() in ['examine', '6']:
        player_examine(action.lower())
    elif action.lower() in ['fight', 'run', 'use_key', 'use_potion']:
        player_fight(action.lower())


def player_move(action):
    ask = "Where would you like to move to? \n"
    print("1. move_north\n2. move_south\n3. move_west\n4. move_east\n")
    destination_input = input(ask)

    if destination_input in ['up', 'north']:
        print("You've selected..")
        destination = char_pos.ZONENAME[mPlayer.location][UP]
        movement_handler(destination)
    elif destination_input in ['down', 'south']:
        destination = char_pos.ZONENAME[mPlayer.location][char_pos.DOWN]
        movement_handler(destination)
    elif destination_input in ['left', 'west']:
        destination = char_pos.ZONENAME[mPlayer.location][char_pos.LEFT]
        movement_handler(destination)
    elif destination_input in ['right', 'east']:
        destination = char_pos.ZONENAME[mPlayer.location][char_pos.RIGHT]
        movement_handler(destination)


def player_examine(action):
    if char_pos.ZONENAME[mPlayer.location][char_pos.SOLVED]:
        print("You've already exhausted this zone.")
    else:
        print("You can trigger a quest here")


def player_fight(action):
    # ask = "What do you want to do? \n"
    print("1. Fight")
    print("2. Run")
    print("3. Use Potion")
    ask = input("What do you want to do? \n")
    player_fight_list = []


def movement_handler(destination):
    print("\n" + "You have moved to the" + destination + ".")
    mPlayer.location = destination
    print_location()


def setup_game():
    clear()

    # Collect names
    name_input = "Please Enter your Name!!\n"
    for player_char in name_input:
        sys.stdout.write(player_char)
        sys.stdout.flush()
        time.sleep(0.05)

    player_name = input("> ")
    mPlayer.name = player_name
    os.system('cls')
    main_game_start()


def main_game_start():
    while mPlayer.game_over is False:
        prompt()

